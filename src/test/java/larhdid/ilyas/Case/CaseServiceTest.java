package larhdid.ilyas.Case;

import larhdid.ilyas.Case.dto.CaseDto;
import larhdid.ilyas.Case.entity.Case;
import larhdid.ilyas.Case.exception.CaseNotFoundException;
import larhdid.ilyas.Case.service.CaseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CaseServiceTest {
    @Autowired
    private CaseService caseService;

    @Test
    public void getCaseTest() throws CaseNotFoundException {
        Case savedCase = caseService.saveCase(new CaseDto(null,LocalDateTime.now(),LocalDateTime.now(), "test title", "test Desc"));
        Case c = caseService.getCase(savedCase.getCaseId());
        assertEquals(c.getTitle(), c.getTitle());
    }
    @Test
    public void updateCaseTest() throws CaseNotFoundException {
        Case savedCase = caseService.saveCase(new CaseDto(null,LocalDateTime.now(),LocalDateTime.now(), "test title", "test Desc"));
        Case c = caseService.updateCase(savedCase.getCaseId(), new CaseDto(null, null,LocalDateTime.now(), "test title updated", "test Desc"));
        assertEquals(c.getTitle(),"test title updated");
    }

}
