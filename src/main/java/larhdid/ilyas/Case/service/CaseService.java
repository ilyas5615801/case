package larhdid.ilyas.Case.service;

import larhdid.ilyas.Case.dto.CaseDto;
import larhdid.ilyas.Case.entity.Case;
import larhdid.ilyas.Case.exception.CaseNotFoundException;
import larhdid.ilyas.Case.repository.CaseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CaseService {
    private final CaseRepository caseRepository;

    public Case getCase(Long id) throws CaseNotFoundException {
        return caseRepository.findById(id).orElseThrow(() -> new CaseNotFoundException(String.format("Error : Case with id %d does not exist!",id)));
        /*Optional<Case> c = caseRepository.findById(id);
        if(c.isPresent()){
            return c.get();
        }
        throw new IllegalStateException(String.format("Error : Case with id %d does not exist!",id));*/
    }

    public Case updateCase(Long id, CaseDto caseDto) throws CaseNotFoundException {
        Case updatedCase = getCase(id);
        updatedCase.setTitle(caseDto.getTitle());
        updatedCase.setDescription(caseDto.getDescription());
        updatedCase.setLastUpdateDate(LocalDateTime.now());
        return caseRepository.save(updatedCase);
    }

    public Case saveCase(CaseDto caseDto){
        Case newCase = new Case(LocalDateTime.now(),LocalDateTime.now(), caseDto.getTitle(), caseDto.getDescription());
        return caseRepository.save(newCase);
    }

    public void deleteCase(Long id){
        caseRepository.deleteById(id);
    }

}
