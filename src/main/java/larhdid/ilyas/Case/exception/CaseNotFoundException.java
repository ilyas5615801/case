package larhdid.ilyas.Case.exception;

public class CaseNotFoundException extends Exception{
    public CaseNotFoundException(String message) {
        super(message);
    }
}
