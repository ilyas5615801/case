package larhdid.ilyas.Case.controller;

import larhdid.ilyas.Case.dto.CaseDto;
import larhdid.ilyas.Case.entity.Case;
import larhdid.ilyas.Case.exception.CaseNotFoundException;
import larhdid.ilyas.Case.service.CaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cases")
public class CaseController {
    private final CaseService caseService;

    @GetMapping("/{id}")
    public Case getCase(@PathVariable("id") Long id) throws CaseNotFoundException {
        return caseService.getCase(id);
    }

    @PostMapping
    public Case saveCase(@RequestBody CaseDto caseDto){
        return caseService.saveCase(caseDto);
    }

    @PutMapping("/{id}")
    public Case updateCase(@PathVariable("id") Long id, @RequestBody CaseDto caseDto) throws CaseNotFoundException {
        return caseService.updateCase(id, caseDto);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCase(@PathVariable("id") Long id){
        caseService.deleteCase(id);
        return ResponseEntity.ok(String.format("Case with id %d has been deleted !",id));
    }

}
