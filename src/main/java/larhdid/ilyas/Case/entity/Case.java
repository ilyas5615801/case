package larhdid.ilyas.Case.entity;

import jakarta.annotation.Generated;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Table(name = "cases")
public class Case {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long caseId;
    private LocalDateTime creationDate;
    private LocalDateTime lastUpdateDate;
    private String title;
    @Column(length = 2056)
    private String description;

    public Case(LocalDateTime creationDate, LocalDateTime lastUpdateDate, String title, String description) {
        this.creationDate = creationDate;
        this.lastUpdateDate = lastUpdateDate;
        this.title = title;
        this.description = description;
    }
}
